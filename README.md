# SKF Frontend test

## How to run the app

The app is very simple to run
You'll only need to run:

- yarn
- yarn start

Aaaand... ✨Magic✨

## Notes

The first part of the test can be tested in the left side of the screen, if needed I can provide the code separately
The app uses CRACO(Create React APP Configuration Overwrite) to overwrite some of CRA settings
It's completely written in TS and uses custom hooks over Container/Component architecture
It has absolute pathing setup with craco alias and tsconfig (Goodbye giant imports)
Prettier and eslint are enabled, so no code w/ no linting will not pass the quality control

... and a bunch of other cool stuff!

## Next Steps

Implement testing
Implement CI/CD
