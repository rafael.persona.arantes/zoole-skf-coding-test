import "react-loading-skeleton/dist/skeleton.css";
import s from "./styles.module.css";
import { Menu } from "components/Menu";
import { useSelectedNodes } from "hooks/useSelectedNodes";
import { Routes, Route } from "react-router-dom";
import { useMenuFetch } from "./hooks/useMenuFetch";
import { getNestedOptions } from "./utils/getNestedOptions";
import { Header } from "./components/Header";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import { MockPage } from "./pages/MockPage";

const App = () => {
  const [selectedOptions, setSelectedOptions] = useSelectedNodes();
  const options = useMenuFetch();

  return (
    <div className={s.App}>
      <Header appName="My APP" />
      <div className="flex">
        <div className={`h-screen ${s.menuContainer}`}>
          {options?.length ? (
            <Menu
              menuOptions={options}
              selectedOptions={selectedOptions}
              setSelectedOptions={setSelectedOptions}
            />
          ) : (
            <div className={s.skeletonWrapper}>
              <SkeletonTheme baseColor="#24354c" highlightColor="#3f5674">
                <Skeleton count={10} />
              </SkeletonTheme>
            </div>
          )}
        </div>
        {options ? (
          <Routes>
            {getNestedOptions(options).map((option) => (
              <Route
                key={option.id}
                path={`/${option.id}`}
                element={<MockPage pageText={option.name} />}
              />
            ))}
            <Route
              path={`/`}
              element={<MockPage pageText="Nothing Selected" />}
            />
          </Routes>
        ) : (
          <div className="w-full flex items-center justify-center">
            <div className={`${s.blockCenter} w-1/5 pt-3`}>
              <Skeleton count={1} />
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default App;
