import s from "./styles.module.css";
type HeaderProps = {
  appName: string;
};
const Header = ({ appName }: HeaderProps) => {
  return (
    <header className={s.header}>
      <h3>{appName}</h3>
    </header>
  );
};

export default Header;
