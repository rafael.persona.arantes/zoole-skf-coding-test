import { useCallback } from "react";
import { MenuType } from "hooks/useSelectedNodes";
import Menu from "../Menu";
import { MenuIcon } from "../MenuIcon";
import { MenuIcons, MenuOption } from "../types";
import { Link, useLocation } from "react-router-dom";
import s from "./styles.module.css";

type MenuNodeOpenProps = {
  menuOption: MenuOption;
  setSelectedOptions: (id: string) => void;
  selectedOptions: MenuType;
};

const MenuNodeOpen = ({
  menuOption: { children, name, id },
  setSelectedOptions,
  selectedOptions,
}: MenuNodeOpenProps) => {
  const location = useLocation();
  const handleClick = useCallback(() => {
    setSelectedOptions(id);
  }, [id, setSelectedOptions]);
  return (
    <li className={`pl-8 ${s.openOption}`}>
      <span
        className={`${s.menuOpt} flex ${
          location.pathname.replace("/", "") === id ? s.active : ""
        }`}
      >
        <div onClick={handleClick} className="mr-2">
          <MenuIcon iconName={MenuIcons.Open} />
        </div>
        <Link to={`/${id}`}>{name}</Link>
      </span>
      <Menu
        menuOptions={children}
        selectedOptions={selectedOptions}
        setSelectedOptions={setSelectedOptions}
      />
    </li>
  );
};

export default MenuNodeOpen;
