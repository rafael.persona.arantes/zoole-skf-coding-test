import { useCallback } from "react";
import { MenuIcon } from "../MenuIcon";
import { MenuIcons, MenuOption } from "../types";
import { Link, useLocation } from "react-router-dom";
import s from "./styles.module.css";
type MenuNodeOpenProps = {
  menuOption: MenuOption;
  setSelectedOptions: (id: string) => void;
  icon: MenuIcons;
};

const MenuNodeClosed = ({
  menuOption: { name, id },
  setSelectedOptions,
  icon,
}: MenuNodeOpenProps) => {
  const location = useLocation();
  const handleClick =
    icon === MenuIcons.Empty
      ? () => {
          return;
        }
      : useCallback(() => setSelectedOptions(id), [id, setSelectedOptions]);

  const animateEmpty = icon === MenuIcons.Empty ? s.appear : "";

  return (
    <li className={`pl-8 ${s.closedOption}`}>
      <span
        className={`${s.menuOpt} flex ${
          location.pathname.replace("/", "") === id ? s.active : ""
        } ${animateEmpty}`}
      >
        <div onClick={handleClick} className="mr-2">
          <MenuIcon iconName={icon} />
        </div>
        <Link to={`/${id}`}>{name}</Link>
      </span>
    </li>
  );
};

export default MenuNodeClosed;
