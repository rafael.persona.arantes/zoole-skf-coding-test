export enum MenuIcons {
  Open,
  Closed,
  Empty,
}

export type MenuOption = {
  id: string;
  name: string;
  children?: MenuOption[];
};
