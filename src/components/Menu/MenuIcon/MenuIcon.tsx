import { MenuIcons } from "../types";
import { FaAngleRight, FaAngleDown } from "react-icons/fa";
import s from "./styles.module.css";
type MenuIconProps = {
  iconName: MenuIcons;
};

const MenuIcon = ({ iconName }: MenuIconProps) => {
  const iconOptions = {
    [MenuIcons.Open]: () => <FaAngleDown className="mt-1 pointer" />,
    [MenuIcons.Closed]: () => (
      <FaAngleRight className={`mt-1 ${s.slideRight} pointer`} />
    ),
    [MenuIcons.Empty]: () => (
      <div style={{ height: 16, width: 16 }} className="mt-1 pointer" />
    ),
  };

  return iconOptions[iconName]();
};

export default MenuIcon;
