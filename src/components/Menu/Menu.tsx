import { MenuType } from "hooks/useSelectedNodes";
import { MenuNodeClosed, MenuNodeOpen } from "./MenuNode";
import { MenuIcons, MenuOption } from "./types";

interface MenuProps {
  menuOptions: MenuOption[] | undefined;
  selectedOptions: MenuType;
  setSelectedOptions: (id: string) => void;
}

const Menu = ({
  menuOptions,
  selectedOptions,
  setSelectedOptions,
}: MenuProps) => (
  <ul>
    {menuOptions?.map((menuOption) => {
      const hasChildren = menuOption?.children?.length;
      return hasChildren && selectedOptions[menuOption.id] ? (
        <MenuNodeOpen
          key={menuOption.id}
          menuOption={menuOption}
          setSelectedOptions={setSelectedOptions}
          selectedOptions={selectedOptions}
        />
      ) : (
        <MenuNodeClosed
          key={menuOption.id}
          menuOption={menuOption}
          icon={hasChildren ? MenuIcons.Closed : MenuIcons.Empty}
          setSelectedOptions={setSelectedOptions}
        />
      );
    })}
  </ul>
);
export default Menu;
