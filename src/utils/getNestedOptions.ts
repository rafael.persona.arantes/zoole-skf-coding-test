import { MenuOption } from "src/components/Menu/types";

type NestedOptions = {
  [key: string]: string;
};

export const getNestedOptions = (menuOptions: MenuOption[]) => {
  let options: NestedOptions[] = [];
  menuOptions.forEach((menuOption) => {
    if (menuOption.children) {
      options = options.concat(getNestedOptions(menuOption.children));
    }
    return options.push({ id: menuOption.id, name: menuOption.name });
  });
  return options;
};
