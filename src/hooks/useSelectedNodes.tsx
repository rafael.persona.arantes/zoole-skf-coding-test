import { useReducer, useState } from "react";

export type MenuType = {
  [key: string]: boolean;
};

type ActionType = {
  payload: string;
};

const nodeReducer = (state: MenuType, { payload }: ActionType) => {
  return {
    ...state,
    [payload]:
      state[payload] === undefined || state[payload] === false ? true : false,
  };
};

type HookReturn = [
  MenuType,
  (id: string) => void,
  string,
  (id: string) => void
];

export const useSelectedNodes = (): HookReturn => {
  // Hook with redux or any other global store (Context, Mobx..)
  const [selectedOptions, dispatch] = useReducer(nodeReducer, {});
  const [selectedMenuOption, setSelectedMenuOption] = useState("");
  const setSelectedNode = (id: string) => dispatch({ payload: id });

  return [
    selectedOptions,
    setSelectedNode,
    selectedMenuOption,
    setSelectedMenuOption,
  ];
};
