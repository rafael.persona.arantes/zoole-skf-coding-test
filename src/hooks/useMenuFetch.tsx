import { useEffect, useState } from "react";
import { getMenu } from "src/api";
import { MenuOption } from "components/Menu/types";

export const useMenuFetch = () => {
  const [menu, setMenu] = useState<undefined | MenuOption[]>(undefined);

  useEffect(() => {
    getMenu()
      .then(setMenu)
      .catch(() => {
        alert("Something unfortunate happened, try again later :(");
        setMenu(undefined);
      });
  }, []);

  return menu;
};
