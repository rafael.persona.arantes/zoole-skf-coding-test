import { MenuOption } from "components/Menu/types";
import { FETCH_MENU_DATA } from "./urls";

const getTree = async (): Promise<MenuOption[]> => {
  const options = await fetch(FETCH_MENU_DATA);
  const { data } = await options.json();
  return data as Promise<MenuOption[]>;
};

export default getTree;
