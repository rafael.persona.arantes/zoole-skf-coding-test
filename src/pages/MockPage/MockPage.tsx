type PageProps = {
  pageText: string;
};
const Page = ({ pageText }: PageProps) => {
  return (
    <section className="w-full flex items-center justify-center">
      <h2 className="text-center text-xl mt-2">{pageText}</h2>
    </section>
  );
};

export default Page;
