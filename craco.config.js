const path = require('path')

module.exports = {
    webpack: {
        alias: {
            components: path.resolve(__dirname, './src/components/'),
            api: path.resolve(__dirname, './src/api/'),
            hooks: path.resolve(__dirname, './src/hooks/'),
            src: path.resolve(__dirname, './src/'),
        },
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
    },
}